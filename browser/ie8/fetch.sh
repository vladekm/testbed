#!/bin/sh

set -e 

if [ -e installed ] 
then
    echo "Installation already complete. Please check your Virtualbox."
    exit 1
fi

if [ -e IE8.Win7.For.MacVirtualBox.part1.sfx ] 
then
    echo "Files already downloaded, skipping."
else
    curl -O -L "https://www.modern.ie/vmdownload?platform=mac&virtPlatform=virtualbox&browserOS=IE8-Win7&parts=4&filename=VMBuild_20131127/VirtualBox/IE8_Win7/Mac/IE8.Win7.For.MacVirtualBox.part{1.sfx,2.rar,3.rar,4.rar}"
fi

if [ -e "IE8 - Win7.ova" ]
then 
    echo "Already extracted the .ova file, skipping."
else
    chmod +x *.sfx
    ./*.sfx
    echo "Extraction complete."
    echo
    echo "Please follow the wizard to import the disk with the default settings."
    echo
    echo "After import is complete:"
    echo  
    echo "1. Run these commands (replacing en0 with the name of the adapter you want to bridge):"
    echo '    VBoxManage sharedfolder add "IE8 - Win7" --name share --hostpath ${PWD}/share --automount'
    echo '    VBoxManage modifyvm "IE8 - Win7" --nic2 bridged  --bridgeadapter1 en0'
    echo
    echo "2. Clean up the downloaded files, they aren't needed once import is finished"
    open IE8\ -\ Win7.ova 
fi

mkdir -p share
cd share
if [ ! -e "IEDriverServer.exe" ]
then
    curl -O -L http://selenium-release.storage.googleapis.com/2.43/IEDriverServer_Win32_2.43.0.zip
    unzip IEDriverServer_Win32_2.43.0.zip
fi

if [ ! -e "selenium-server-standalone-2.43.1.jar" ]
then 
    curl -O -L http://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar
fi

if [ ! -e "jre-7u67-windows-i586.exe" ]
then
    curl -o jre-7u67-windows-i586.exe -O -L http://javadl.sun.com/webapps/download/AutoDL?BundleId=95123
fi

cd ..
touch installed
