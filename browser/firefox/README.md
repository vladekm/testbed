This is where experimental Firefox extensions to generate a console.log file in this directory.

# Install

Fetch some extensions into this directory:

~~~
curl -o firebug-2.0.4-fx.xpi -O -L https://addons.mozilla.org/firefox/downloads/file/274666/firebug-2.0.4-fx.xpi?src=version-history
curl -o consoleExport-0.5b6.xpi -O -L "https://fbug.googlecode.com/issues/attachment?aid=51820019000&name=consoleExport-0.5b6.xpi&token=ABZ6GAco4-qKm1tXq9FX3adFHofkIvt6ng%3A1412794416752"
~~~

# Run

Now set the environment variable `BROWSER` to `firefox` and set the
`FIREFOX_FILES_DIR` to `browser/firefox` if you want to see a `console.log`
file in that directory.

~~~
cd ../../
BROWSER=firefox FIREFOX_FILES_DIR=browser/firefox env/bin/python test.py
~~~
